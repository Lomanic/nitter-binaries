# Nitter binary releases

Binaries built on CI so you don't have to install the whole nim toolchain.

```bash
curl -L "https://gitlab.com/Lomanic/nitter-binaries/-/jobs/artifacts/master/download?job=linux_armhf" -o nitter.zip # for linux arm
curl -L "https://gitlab.com/Lomanic/nitter-binaries/-/jobs/artifacts/master/download?job=linux_arm64" -o nitter.zip # for linux arm64
curl -L "https://gitlab.com/Lomanic/nitter-binaries/-/jobs/artifacts/master/download?job=linux_amd64" -o nitter.zip # for linux x64
curl -L "https://gitlab.com/Lomanic/nitter-binaries/-/jobs/artifacts/master/download?job=windows_amd64" -o nitter.zip # for windows x64
unzip nitter.zip
curl -L https://github.com/zedeus/nitter/raw/master/nitter.conf -o nitter/nitter.conf
cd nitter
# modify nitter.conf, you also need a running redis
./nitter
```
